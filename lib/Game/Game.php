<?php
declare(strict_types=1);

namespace Gamecli\Game;

use Gamecli\Entity\WildBeast;
use Gamecli\Entity\Mortal;
use Gamecli\Entity\Orderus;
use Gamecli\Game\Engine\GameEngine;

/**
 * Class Game
 * @package Gamecli\Game
 */
class Game extends GameEngine
{
    /**
     * Game constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Sets up a game, registers combatants
     */
    public function setUpGame(): void
    {
        $this->registeredCombatants = [new WildBeast($this->combatLog), new Orderus($this->combatLog)];
        $this->combatantsAlive = $this->registeredCombatants;
    }

    /**
     * Starts a game
     */
    public function gameStart(): void
    {
        $this->setUpGame();
        $actionOrder = $this->rollHelper->rollForInitiative($this->registeredCombatants);

        for ($roundCount = 1; $roundCount < 20; $roundCount++){
            if ($this->areAllDeadExceptOne())
                break;

            $this->cliPrinter->display("Round: " . $roundCount);
            $this->round($actionOrder);

            $this->combatLog->displayAndDump();
        }

        if ($roundCount === 20){
            $this->cliPrinter->display('Game ended without winner');
        }
    }

    /**
     * Single round of game
     * @param Mortal[] $order
     */
    public function round(array $order): void
    {
        foreach ($order as $combatant){
            if (!in_array($combatant, $this->combatantsAlive))
                continue;

            $combatant->setTarget($this->selectTargetForAttacker($combatant));
            $combatant->attack();

            $this->didTargetDie($combatant->target);
        }
    }

    /**
     * Selects target for an attacker, specific for current game
     * @param Mortal $combatant
     * @return mixed
     */
    public function selectTargetForAttacker(Mortal $combatant): Mortal
    {
        $key = array_search($combatant, $this->combatantsAlive);
        $aliveCombatants = $this->combatantsAlive;
        unset($aliveCombatants[$key]);

        $randomKeyOfTarget = array_rand($aliveCombatants);
        return $aliveCombatants[$randomKeyOfTarget];
    }
}