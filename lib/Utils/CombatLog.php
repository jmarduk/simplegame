<?php
declare(strict_types=1);

namespace Gamecli\Utils;

/**
 * Class CombatLog
 * @package Gamecli\Utils
 */
class CombatLog
{
    public $log;
    public $printer;

    /**
     * CombatLog constructor.
     */
    public function __construct()
    {
        $this->printer = new CliPrinter();
    }

    public function attackMessage($attacker, $attacked): void
    {
        $this->log []= $attacker . ' looks straight into eyes of ' . $attacked . ' and attacks!';
    }

    public function dodgeMessage($name): void
    {
        $this->log []= $name . ' dodges attack!';
    }

    public function receiveDamageMessage($defendersName, $damage, $remainingHealth): void
    {
        $this->log []= $defendersName . ' receives damage equal to ' . $damage . '. His remaining health is: ' . $remainingHealth;
    }

    public function diesMessage($fallen): void
    {
        $this->log []=  $fallen . ' dies!';
    }

    public function magicShieldMessage($name): void
    {
        $this->log []= $name . ' uses magic shield!!';
    }

    public function rapidStrikeMessage($name): void
    {
        $this->log []= $name . ' uses rapid strike!';
    }

    /**
     * Displays whole combat log and dumps it
     */
    public function displayAndDump(): void
    {
        foreach ($this->log as $singleMessage){
            $this->printer->display($singleMessage);
        }
        $this->log = [];
    }
}