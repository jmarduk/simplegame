<?php
declare(strict_types=1);

namespace Gamecli\Entity;

use Gamecli\Entity\Contract\Attackable;
use Gamecli\Entity\Contract\Defendable;
use Gamecli\Entity\Contract\Dieable;
use Gamecli\Utils\CombatLog;

/**
 * Class Mortal
 * @package Gamecli\Entity
 */
class Mortal implements Attackable, Defendable, Dieable
{
    public $name;
    public $health;
    public $strength;
    public $defence;
    public $speed;
    public $luck;
    public $isAlive = true;
    public $target;
    public $combatLog;

    /**
     * Mortal constructor.
     * @param CombatLog $combatLog
     */
    public function __construct(CombatLog $combatLog)
    {
        $this->combatLog = $combatLog;
    }

    public function attack(): void
    {
        $this->combatLog->attackMessage($this->name, $this->target->name);
        $this->strike();
    }

    public function strike(): void
    {
        $this->target->receiveAttack($this->strength);
    }

    /**
     * @param float $enemyHitPower
     */
    public function receiveAttack(float $enemyHitPower): void
    {
        if (rand(0, 100) <= $this->luck){
            $this->combatLog->dodgeMessage($this->name);
        } else {
            $this->receiveHit($enemyHitPower);
        }
    }

    /**
     * @param float $enemyHitPower
     */
    public function receiveHit(float $enemyHitPower): void
    {
        $dmg = $this->evaluateDamage($enemyHitPower);
        $this->receiveDamage($dmg);
    }

    /**
     * @param float $enemyHitPower
     * @return float
     */
    public function evaluateDamage(float $enemyHitPower): float
    {
        return $enemyHitPower - $this->defence;
    }

    /**
     * @param float $dmg
     */
    public function receiveDamage(float $dmg): void
    {
        $this->health = $this->health - $dmg;
        $this->combatLog->receiveDamageMessage($this->name, $dmg, $this->health);
        $this->isDead();
    }

    /**
     * Makes guy dead
     */
    public function isDead(): void
    {
        if ($this->health <= 0){
            $this->combatLog->diesMessage($this->name);
            $this->isAlive = false;
        }
    }

    /**
     * @param Defendable $target
     */
    public function setTarget(Defendable $target): void
    {
        $this->target = $target;
    }
}