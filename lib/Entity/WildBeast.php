<?php
declare(strict_types=1);

namespace Gamecli\Entity;

use Gamecli\Utils\CombatLog;

/**
 * Class WildBeast
 * @package Gamecli\Entity
 */
class WildBeast extends Mortal
{
    /**
     * WildBeast constructor.
     * @param CombatLog $combatLog
     */
    public function __construct(CombatLog $combatLog)
    {
        parent::__construct($combatLog);
        $this->name = 'Wild Beast';
        $this->health = rand(60, 90);
        $this->strength = rand(60, 90);
        $this->defence = rand(40, 60);
        $this->speed = rand(40, 60);
        $this->luck = rand(25, 40);
    }
}