<?php
declare(strict_types=1);

namespace Gamecli\Entity\Contract;

/**
 * Interface Dieable
 * @package Gamecli\Entity\Contract
 */
interface Dieable
{
    /**
     * Checks if ones hp is below zero and if it is makes is 'dead'
     */
    public function isDead(): void;
}