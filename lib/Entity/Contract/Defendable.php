<?php
declare(strict_types=1);

namespace Gamecli\Entity\Contract;

/**
 * Interface Defendable
 * @package Gamecli\Entity\Contract
 */
interface Defendable
{
    /**
     * Receives an attack from an opponent
     * @param float $enemyHitPower
     * @return mixed
     */
    public function receiveAttack(float $enemyHitPower): void;

    /**
     * Receives damage that is subtracted from hp
     * @param float $dmg
     */
    public function receiveDamage(float $dmg): void;

    /**
     * Receives direct hit causing damage
     * @param float $enemyHitPower
     * @return mixed
     */
    public function receiveHit(float $enemyHitPower): void;

    /**
     * Evaluates damage based on parameters
     * @param float $enemyHitPower
     * @return float
     */
    public function evaluateDamage(float $enemyHitPower): float;
}