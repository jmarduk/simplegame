Przed odpaleniem aplikacji nalezy zrobic composer install.

Jak odpalic:
```
./gamecli
```

Testy: 
```
./vendor/bin/phpunit tests/
```

Podszedlem do zadanka tak aby w miare w latwy sposob moznaby aplikacje rozszerzac,
aktualnie apka bylaby w stanie 'zahostowac' gre kazdy na kazdego z dowolna liczba przeciwnikow.
Punktem startu jest methoda ```gameStart```  w klasie ```Game``` ktora rozpoczyna gre ustawiajac liczbe rund na 20,
 kazda runda toczy sie w odrebnej metodzie o nazwie ```round```.
Ustawienie przeciwnikow jest zahardcodowane w metodzie ```setUpGame``` w klasie ```Game```.
