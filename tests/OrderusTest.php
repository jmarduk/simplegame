<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Gamecli\Utils\CombatLog;
use Gamecli\Entity\Orderus;
use Gamecli\Entity\WildBeast;

/**
 * Class OrderusTest
 */
class OrderusTest extends TestCase
{
    public function testRapidStrike(): void
    {
        $orderus = new Orderus(new CombatLog());
        $orderus->strength = 100;

        $beast = new WildBeast(new CombatLog());
        $beast->health = 500;
        $beast->luck = 0;
        $beast->defence = 0;

        $orderus->setTarget($beast);
        $orderus->rapidStrike();

        $this->assertEquals(300, $beast->health);
    }

    public function testMagicShield(): void
    {
        $orderus = new Orderus(new CombatLog());
        $orderus->defence = 0;
        $orderus->health = 500;
        $orderus->magicShield(100);

        $this->assertEquals(450, $orderus->health);
    }

}