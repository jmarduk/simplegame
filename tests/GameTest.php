<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Gamecli\Utils\CombatLog;
use Gamecli\Entity\Orderus;
use Gamecli\Entity\WildBeast;
use Gamecli\Game\Game;

/**
 * Class GameTest
 */
class GameTest extends TestCase
{
    public function testVictory(): void
    {
        $game = new Game();
        $game->setUpGame();

        unset($game->combatantsAlive[0]);
        $res = $game->areAllDeadExceptOne();

        $this->assertTrue($res);
    }

    public function testDidTargetDie(): void
    {
        $orderus = new Orderus(new CombatLog());
        $beast = new WildBeast(new CombatLog());

        $orderus->setTarget($beast);
        $beast->luck = 0;
        $orderus->strength = 1000000;
        $orderus->attack();

        $this->assertFalse($beast->isAlive);
    }

    public function testSelectRandomTarget(): void
    {
        $game = new Game();
        $game->setUpGame();

        $target = $game->selectTargetForAttacker($game->combatantsAlive[0]);
        $this->assertEquals($game->combatantsAlive[1], $target);
    }
}