<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Gamecli\Utils\RollHelper;
use Gamecli\Utils\CombatLog;
use Gamecli\Entity\Orderus;
use Gamecli\Entity\WildBeast;

/**
 * Class RollHelperTest
 */
class RollHelperTest extends TestCase
{
    public function testInitiativeForSameSpeedCombatants(): void
    {
        $helper = new RollHelper();

        $orderus = new Orderus(new CombatLog());
        $orderus->speed = 1;
        $orderus->luck = 1;

        $beast = new WildBeast(new CombatLog());
        $beast->speed = 1;
        $beast->luck = 10;

        $attackOrder = $helper->rollForInitiative([$orderus, $beast]);

        $this->assertEquals('Wild Beast', $attackOrder[0]->name);
    }

    public function testInitiativeWithDifferentSpeed(): void
    {
        $helper = new RollHelper();

        $orderus = new Orderus(new CombatLog());
        $orderus->speed = 1;

        $beast = new WildBeast(new CombatLog());
        $beast->speed = 10;

        $attackOrder = $helper->rollForInitiative([$orderus, $beast]);

        $this->assertEquals('Wild Beast', $attackOrder[0]->name);
    }

}